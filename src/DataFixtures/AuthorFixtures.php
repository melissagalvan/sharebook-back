<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\DataFixtures\BookFixtures;
use App\Entity\Author;

class AuthorFixtures extends Fixture implements DependentFixtureInterface
{

    // public const ALEXANDRE_DUMAS_REFERENCE = 'alexandreDumas';
    // public const GUY_DE_MAUPASSANT_REFERENCE = 'guyDeMaupassant';
    // public const ALFRED_DE_MUSSET_REFERENCE = 'alfredDeMusset';
    // public const EMILE_ZOLA_REFERENCE = 'emileZola';
    
    public function load(ObjectManager $manager)
    {
        $alexandreDumas = new Author();
        $alexandreDumas->setFullname('Alexandre Dumas');
        $alexandreDumas->addBook($this->getReference(BookFixtures::LES_3_MOUSQUETAIRES_REFERENCE));
        
        $guyDeMaupassant = new Author();
        $guyDeMaupassant->setFullname('Guy De Maupassant');
        $guyDeMaupassant->addBook($this->getReference(BookFixtures::CONTES_DU_JOUR_ET_DE_LA_NUIT_REFERENCE));

        $alfredDeMusset = new Author();
        $alfredDeMusset->setFullname('Alfred De Musset');
        $alfredDeMusset->addBook($this->getReference(BookFixtures::LES_CAPRICES_DE_MARIANNE_REFERENCE));
        $alfredDeMusset->addBook($this->getReference(BookFixtures::ON_NE_BADINE_PAS_AVEC_LAMOUR_REFERENCE));

        $emileZola = new Author();
        $emileZola->setFullname('Emile Zola');
        $emileZola->addBook($this->getReference(BookFixtures::NANA_REFERENCE));

        $manager->persist($alexandreDumas);
        $manager->persist($guyDeMaupassant);
        $manager->persist($alfredDeMusset);
        $manager->persist($emileZola);

        $manager->flush();

        // $this->addReference(self::ALEXANDRE_DUMAS_REFERENCE, $alexandreDumas);
        // $this->addReference(self::GUY_DE_MAUPASSANT_REFERENCE, $guyDeMaupassant);
        // $this->addReference(self::ALFRED_DE_MUSSET_REFERENCE, $alfredDeMusset);
        // $this->addReference(self::EMILE_ZOLA_REFERENCE, $emileZola);
    }

    public function getDependencies()
    {
        return array(
            BookFixtures::class,
        );
    }
}
