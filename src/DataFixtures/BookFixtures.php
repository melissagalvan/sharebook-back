<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\DataFixtures\CategoryFixtures;
use App\Entity\Book;
use App\Service\UploadService;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class BookFixtures extends Fixture implements DependentFixtureInterface
{
    private $uploader;
    private $filesystem;

    public const LES_3_MOUSQUETAIRES_REFERENCE = 'les3Mousquetaires';
    public const CONTES_DU_JOUR_ET_DE_LA_NUIT_REFERENCE = 'contesDuJourEtDeLaNuit';
    public const LES_CAPRICES_DE_MARIANNE_REFERENCE = 'lesCapricesDeMarianne';
    public const ON_NE_BADINE_PAS_AVEC_LAMOUR_REFERENCE = 'onNeBadinePasAvecLamour';
    public const NANA_REFERENCE = 'nana';

    public function __construct(UploadService $uploader, Filesystem $filesystem)
    {
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
    }

    public function load(ObjectManager $manager)
    {
        $this->filesystem->remove($_ENV['UPLOAD_DIRECTORY']);

        // Books
        $les3Mousquetaires = new Book();
        $les3Mousquetaires->setTitle('Les Trois Mousquetaires');
        $les3Mousquetaires->setExcerpt('Dans Les Trois Mousquetaires revit toute l\'Histoire: le Moyen Age ...');
        $les3Mousquetaires->addCategory($this->getReference(CategoryFixtures::ROMAN_HISTORIQUE_REFERENCE));
        // $this->filesystem->copy(__DIR__ . '/../../assets/images/les3Mousquetaires.png', __DIR__ . '/../../assets/images/les3MousquetairesImage.png');
        // $les3MousquetairesImage = $this->uploader->upload(new File(__DIR__ . '/../../assets/images/les3MousquetairesImage.png'));
        // $les3Mousquetaires->setImage($les3MousquetairesImage);
        $les3Mousquetaires->setImage('https://m.media-amazon.com/images/I/41ld4kI3dKL.jpg');

        $contesDuJourEtDeLaNuit = new Book();
        $contesDuJourEtDeLaNuit->setTitle('Contes du jour et de la nuit');
        $contesDuJourEtDeLaNuit->setExcerpt('En 1884, lorqu\'il publie des Contes, Maupassant est devenu un homme riche et ...');
        $contesDuJourEtDeLaNuit->addCategory($this->getReference(CategoryFixtures::RECUEIL_DE_NOUVELLES_REFERENCE));
        // $this->filesystem->copy(__DIR__ . '/../../assets/images/contesDuJourEtDeLaNuit.png', __DIR__ . '/../../assets/images/contesDuJourEtDeLaNuitImage.png');
        // $contesDuJourEtDeLaNuitImage = $this->uploader->upload(new File(__DIR__ . '/../../assets/images/contesDuJourEtDeLaNuitImage.png'));
        // $contesDuJourEtDeLaNuit->setImage($contesDuJourEtDeLaNuitImage);
        $contesDuJourEtDeLaNuit->setImage('https://images-na.ssl-images-amazon.com/images/I/512qtNiQQ3L._SX301_BO1,204,203,200_.jpg');

        $lesCapricesDeMarianne = new Book();
        $lesCapricesDeMarianne->setTitle('Les Caprices de Marianne');
        $lesCapricesDeMarianne->setExcerpt('Dans une Naples romantique se déroule l\'éternel jeu de l\'amour et de la mort. Coelio le passionné ..');
        $lesCapricesDeMarianne->addCategory($this->getReference(CategoryFixtures::DRAME_REFERENCE));
        // $this->filesystem->copy(__DIR__ . '/../../assets/images/lesCapricesDeMarianne.png', __DIR__ . '/../../assets/images/lesCapricesDeMarianneImage.png');
        // $lesCapricesDeMarianneImage = $this->uploader->upload(new File(__DIR__ . '/../../assets/images/lesCapricesDeMarianneImage.png'));
        // $lesCapricesDeMarianne->setImage($lesCapricesDeMarianneImage);
        $lesCapricesDeMarianne->setImage('https://images-na.ssl-images-amazon.com/images/I/51YHVkro%2B3L.jpg');

        $onNeBadinePasAvecLamour = new Book();
        $onNeBadinePasAvecLamour->setTitle('On ne badine pas avec l\'amour');
        $onNeBadinePasAvecLamour->setExcerpt('Deux êtres se déchirent avant même de s\'aimer. Des masques d\'orgueil s\'affrontent et jouent ...');
        $onNeBadinePasAvecLamour->addCategory($this->getReference(CategoryFixtures::DRAME_ROMANTIQUE_REFERENCE));
        // $this->filesystem->copy(__DIR__ . '/../../assets/images/onNeBadinePasAvecLamour.png', __DIR__ . '/../../assets/images/onNeBadinePasAvecLamourImage.png');
        // $onNeBadinePasAvecLamourImage = $this->uploader->upload(new File(__DIR__ . '/../../assets/images/onNeBadinePasAvecLamourImage.png'));
        // $onNeBadinePasAvecLamour->setImage($onNeBadinePasAvecLamourImage);
        $onNeBadinePasAvecLamour->setImage('https://m.media-amazon.com/images/I/51rhDgN4fmL.jpg');

        $nana = new Book();
        $nana->setTitle('Nana');
        $nana->setExcerpt('Dans les dernières années du Second Empire ...');
        $nana->addCategory($this->getReference(CategoryFixtures::ROMAN_REFERENCE));
        // $this->filesystem->copy(__DIR__ . '/../../assets/images/nana.png', __DIR__ . '/../../assets/images/nanaImage.png');
        // $nanaImage = $this->uploader->upload(new File(__DIR__ . '/../../assets/images/nanaImage.png'));
        // $nana->setImage($nanaImage);
        $nana->setImage('https://images-na.ssl-images-amazon.com/images/I/81CNQzNdfVL.jpg');

        $manager->persist($les3Mousquetaires);
        $manager->persist($contesDuJourEtDeLaNuit);
        $manager->persist($lesCapricesDeMarianne);
        $manager->persist($onNeBadinePasAvecLamour);
        $manager->persist($nana);

        $manager->flush();

        $this->addReference(self::LES_3_MOUSQUETAIRES_REFERENCE, $les3Mousquetaires);
        $this->addReference(self::CONTES_DU_JOUR_ET_DE_LA_NUIT_REFERENCE, $contesDuJourEtDeLaNuit);
        $this->addReference(self::LES_CAPRICES_DE_MARIANNE_REFERENCE, $lesCapricesDeMarianne);
        $this->addReference(self::ON_NE_BADINE_PAS_AVEC_LAMOUR_REFERENCE, $onNeBadinePasAvecLamour);
        $this->addReference(self::NANA_REFERENCE, $nana);

    }

    public function getDependencies()
    {
        return array(
            CategoryFixtures::class,
        );
    }
}