<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\CategoryType;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
* @Route("sharebook-api/categories", name="categories")
*/
class CategoryController extends AbstractController
{
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/add", methods="POST")
     */
    public function addCategory(Request $request, EntityManagerInterface $managerInterface)
    {
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        $form->submit(json_decode($request->getContent(), true));        

        if ($form->isSubmitted() && $form->isValid()) {
            $managerInterface->persist($category);
            $managerInterface->flush();

            return new JsonResponse($this->serializer->serialize($category, 'json'), 201, [], true);
        }

        return $this->json($form->getErrors(true), 400);
    }

    /**
     * @Route(methods="GET")
     */
    public function readAllCategories(CategoryRepository $categoryRepo)
    {
        $categories = $categoryRepo->findAll();
        $json = $this->serializer->serialize($categories, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/{category}", methods="GET")
     */
    public function readOneCategory(Category $category)
    {
        return new JsonResponse($this->serializer->serialize($category, 'json'), 200, [], true);
    }

    /**
     * @Route("/delete/{category}", methods="DELETE")
     */
    public function deleteCategoryById(Category $category, EntityManagerInterface $managerInterface)
    {
        $managerInterface->remove($category);
        $managerInterface->flush();

        return $this->json('', 204);
    }
}
